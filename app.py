#    app.py - Display a splork message of the day
#    Copyright (C) 2020-2022  William R. Moore <william@nerderium.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program.  If not, see <https://www.gnu.org/licenses/>.

import requests
import tkinter as tk
from time import sleep
import threading
import pyautogui
import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter(fmt="%(asctime)s %(name)s.%(levelname)s: %(messages")

handler = logging.StreamHandler(stream=sys.stdout)
handler.setFormatter(formatter)

logger.addHandler(handler)

def update_splork():
    while True:
        try:
            message = get_splork()
            logger.info(message)
            if not message == None:
                splorkVar.set(message)
                sleep(86400)
            else:
                logger.info('Unable to retrieve a splork!')
                sleep(500)
        except:
            sleep(500)

def get_splork():
    r = requests.get('https://splork.nerderium.com/message')
    message = None

    if r and r.status_code == 200:
        message = r.json()['message']

    return message

window = tk.Tk()
window.attributes('-fullscreen', True)
window.title('Passim Display')

splorkVar = tk.StringVar()

splork = tk.Label(window, wraplength=500, justify='center', textvariable=splorkVar, font=('Helvetica', 18))
splork.pack(fill=tk.BOTH, expand=1)

x = threading.Thread(target=update_splork)
x.start()

screen_size = pyautogui.size()
pyautogui.FAILSAFE=False
pyautogui.moveTo(0,screen_size.height,duration=1)

window.mainloop()
